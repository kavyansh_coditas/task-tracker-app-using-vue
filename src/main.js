import { createApp } from 'vue';
import App from './App.vue';


const app = createApp(App);

import TheHeader from './components/TheHeader.vue';

app.component('the-header',TheHeader)

app.mount('#app')